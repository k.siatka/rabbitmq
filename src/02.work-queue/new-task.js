const amqp = require('amqplib/callback_api');
const RMQ_URL = require('../settings').RMQ_URL;

amqp.connect(RMQ_URL, (err, conn) => {

    conn.createChannel((err, ch) => {

        const queueName = 'task-queue';

        const message = process.argv.slice(2).join(' ') || `Test message ${Date.now()}`;

        ch.assertQueue(queueName, { durable: true }); // <-- durable: queue won't be lost even if RabbitMQ restarts

        ch.sendToQueue(queueName, Buffer.from(message), { persistent: true }); // <-- persistent: message won't be lost even if RabbitMQ restarts

        console.log(`Sent '${message}'`);
    });


    setTimeout(() => {
        conn.close();
        process.exit(0);
    }, 500);
});