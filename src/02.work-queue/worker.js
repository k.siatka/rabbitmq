const amqp = require('amqplib/callback_api');
const RMQ_URL = require('../settings').RMQ_URL;

const TASK_TIME = 2500;

amqp.connect(RMQ_URL, (err, conn) => {

    conn.createChannel(function (err, ch) {

        const queueName = 'task-queue';

        ch.assertQueue(queueName, { durable: true });

        ch.prefetch(1); // <-- prefetch: not to give more than one message to a worker at a time

        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queueName);

        ch.consume(queueName, (msg) => {

            console.log(` Received ${msg.content.toString()}`);

            setTimeout(() => {
                console.log(' [x] Done');

                ch.ack(msg); // <-- manual ACK
            }, TASK_TIME);

        }, { noAck: false }); // <-- no auto-ACK
    });

});