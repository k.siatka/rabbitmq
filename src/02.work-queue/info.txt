Work Queue = Task Queue

- avoid doing a resource-intensive task immediately and having to wait for it to complete
- worker process running in the background will pop the tasks and eventually execute the job
- useful in web applications where it's impossible to handle a complex task during a short HTTP request window


example: fair dispatch

              Q      --prefetch=1-- > WORKER1
TASK -> [---------]  --prefetch=1-- > WORKER2
                    ...
                     --prefetch=1-- > WORKERn



