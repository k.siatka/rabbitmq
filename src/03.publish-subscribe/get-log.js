const amqp = require('amqplib/callback_api');
const RMQ_URL = require('../settings').RMQ_URL;

amqp.connect(RMQ_URL, (err, conn) => {

    conn.createChannel((err, ch) => {

        const exName = 'logs';

        ch.assertExchange(exName, 'fanout', { durable: false });

        // --> '': random queue name, exclusive: delete queue on disconnect
        ch.assertQueue('', { exclusive: true }, (err, q) => {

            console.log(` [*] Waiting for messages in ${q.queue}. To exit press CTRL+C`);

            ch.bindQueue(q.queue, exName, '');

            ch.consume(q.queue, (msg) => console.log(` [x] ${msg.content.toString()}`), { noAck: true });

        });
    });

});