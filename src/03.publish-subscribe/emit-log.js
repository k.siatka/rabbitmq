const amqp = require('amqplib/callback_api');
const RMQ_URL = require('../settings').RMQ_URL;

amqp.connect(RMQ_URL, (err, conn) => {

    conn.createChannel((err, ch) => {

        const exName = 'logs';

        const message = process.argv.slice(2).join(' ') || `Test message ${Date.now()}`;

        ch.assertExchange(exName, 'fanout', { durable: false });

        ch.publish(exName, '', Buffer.from(message)); // <-- '': routing key (będzie później)

        console.log(`Sent '${message}'`);
    });


    setTimeout(() => {
        conn.close();
        process.exit(0);
    }, 500);
});