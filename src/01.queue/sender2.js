const amqp = require('amqplib/callback_api');
const RMQ_URL = require('../settings').RMQ_URL;

let counter = 0;

amqp.connect(RMQ_URL, (err, conn) => {

    conn.createChannel((err, ch) => {
        const queueName = 'rabbitmq-queue';

        ch.assertQueue(queueName, { durable: false });

        setInterval(() => {

            const message = `Test message #${++counter}, ${Date.now()}`;

            ch.sendToQueue(queueName, Buffer.from(message));

            console.log(` Sent: '${message}.'`);

        }, 2000);
    });
});