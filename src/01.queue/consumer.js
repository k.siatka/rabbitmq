const amqp = require('amqplib/callback_api');
const RMQ_URL = require('../settings').RMQ_URL;

amqp.connect(RMQ_URL, (err, conn) => {

    conn.createChannel(function (err, ch) {

        const queueName = 'rabbitmq-queue';

        ch.assertQueue(queueName, { durable: false });

        console.log(` [*] Waiting for messages in ${queueName}. To exit press CTRL+C`);

        ch.consume(queueName, (msg) => {

            console.log(` Received ${msg.content.toString()}`);

        }, { noAck: true });
    });

});