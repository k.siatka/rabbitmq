const amqp = require('amqplib/callback_api');
const RMQ_URL = require('../settings').RMQ_URL;

amqp.connect(RMQ_URL, (err, conn) => {

    conn.createChannel((err, ch) => {
        const queueName = 'rabbitmq-queue';

        ch.assertQueue(queueName, { durable: false });

        const message = `Test message ${Date.now()}`;

        ch.sendToQueue(queueName, Buffer.from(message));

        console.log(`Sent '${message}'`);
    });


    setTimeout(() => {
        conn.close();
        process.exit(0);
    }, 500);
});