const amqp = require('amqplib/callback_api');
const RMQ_URL = require('../settings').RMQ_URL;

amqp.connect(RMQ_URL, (err, conn) => {

    conn.createChannel((err, ch) => {

        const exName = 'direct-logs';

        const args = process.argv.slice(2);
        const message = args.slice(1).join(' ') || 'Hello World!';
        const severity = (args.length > 0) ? args[0] : 'info';

        ch.assertExchange(exName, 'direct', { durable: false });

        ch.publish(exName, severity, Buffer.from(message)); // <-- routing key - severity

        console.log(`Sent ${severity}: '${message}'`);
    });


    setTimeout(() => {
        conn.close();
        process.exit(0);
    }, 500);
});