const amqp = require('amqplib/callback_api');
const RMQ_URL = require('../settings').RMQ_URL;

const args = process.argv.slice(2);

if (args.length === 0) {
    console.log(`Usage: get-logs.js [info] [warning] [error]`);
    process.exit(1);
}

amqp.connect(RMQ_URL, (err, conn) => {

    conn.createChannel((err, ch) => {

        const exName = 'direct-logs';

        ch.assertExchange(exName, 'direct', { durable: false });

        ch.assertQueue('', { exclusive: true }, (err, q) => {

            console.log(` [*] Waiting for ${args} messages in ${q.queue}. To exit press CTRL+C`);

            args.forEach((severity) => ch.bindQueue(q.queue, exName, severity));

            ch.consume(q.queue,
                (msg) => console.log(` [x] ${msg.fields.routingKey}: ${msg.content.toString()}`), { noAck: true });

        });
    });

});