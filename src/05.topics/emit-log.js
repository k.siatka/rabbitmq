const amqp = require('amqplib/callback_api');
const RMQ_URL = require('../settings').RMQ_URL;

amqp.connect(RMQ_URL, (err, conn) => {

    conn.createChannel((err, ch) => {

        const exName = 'topic-logs';

        const args = process.argv.slice(2);
        const message = args.slice(1).join(' ') || 'Hello World!';
        const key = (args.length > 0) ? args[0] : 'anonymous.info';

        ch.assertExchange(exName, 'topic', { durable: false });

        ch.publish(exName, key, Buffer.from(message));

        console.log(`Sent ${key}: '${message}'`);
    });


    setTimeout(() => {
        conn.close();
        process.exit(0);
    }, 500);
});