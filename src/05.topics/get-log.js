const amqp = require('amqplib/callback_api');
const RMQ_URL = require('../settings').RMQ_URL;

const args = process.argv.slice(2);

if (args.length === 0) {
    console.log(`Usage: get-logs.js <from>.<severity>`);
    process.exit(1);
}

amqp.connect(RMQ_URL, (err, conn) => {

    conn.createChannel((err, ch) => {

        const exName = 'topic-logs';

        ch.assertExchange(exName, 'topic', { durable: false });

        ch.assertQueue('', { exclusive: true }, (err, q) => {

            console.log(` [*] Waiting for ${args} messages in ${q.queue}. To exit press CTRL+C`);

            args.forEach((key) => ch.bindQueue(q.queue, exName, key));

            ch.consume(q.queue,
                (msg) => console.log(` [x] ${msg.fields.routingKey}: ${msg.content.toString()}`), { noAck: true });

        });
    });

});